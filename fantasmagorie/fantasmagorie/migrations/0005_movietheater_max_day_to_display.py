# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0004_movietheater_extra_foot'),
    ]

    operations = [
        migrations.AddField(
            model_name='movietheater',
            name='max_day_to_display',
            field=models.IntegerField(default=31, verbose_name='Maximum day to display'),
        ),
    ]
