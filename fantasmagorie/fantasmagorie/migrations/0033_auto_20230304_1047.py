# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2023-03-04 09:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0032_auto_20230304_1022'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='broadcasttag',
            options={'ordering': ('order', 'short_name', 'name'), 'verbose_name': 'Broadcast tag', 'verbose_name_plural': 'Broadcast tags'},
        ),
        migrations.AlterModelOptions(
            name='movietag',
            options={'ordering': ('order', 'short_name', 'name'), 'verbose_name': 'Movie tag', 'verbose_name_plural': 'Movie tags'},
        ),
        migrations.AddField(
            model_name='broadcasttag',
            name='order',
            field=models.IntegerField(default=10, verbose_name='Order'),
        ),
        migrations.AddField(
            model_name='movietag',
            name='order',
            field=models.IntegerField(default=10, verbose_name='Order'),
        ),
    ]
