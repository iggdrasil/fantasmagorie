import datetime
from enum import Enum
#import facebook
import re

from django.contrib.flatpages.models import FlatPage as BaseFlatPage
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings
from django.urls import reverse
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.html import escape, mark_safe
from django.utils.translation import ugettext_lazy as _

try:
    from oauth2client.contrib.django_util.models import CredentialsField
except:
    CredentialsField = None

from newsletter.models import Newsletter


PAGE_TYPES = (('html', _('HTML')),
              ('md', _('Markdown')))


if CredentialsField:
    class CredentialsModel(models.Model):
        credential = CredentialsField()


class FlatPage(BaseFlatPage):
    page_type = models.CharField(_("Type"), choices=PAGE_TYPES, default='html',
                                 max_length=10)
    for_staff = models.BooleanField(_("Page for staff only"), default=False)

    class Meta:
        verbose_name = _("Page - template")
        verbose_name_plural = _("Page - templates")

    def get_absolute_url(self):
        return reverse('flatpage-link', args=[self.url])


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        choices = list()
        # Loop threw defined enums
        for item in cls:
            choices.append((item.value, item.name))
        return tuple(choices)

    def __str__(self):
        return self.name

    def __int__(self):
        return _(self.value)


slug_help = _("The slug is the standardized version of the name. It contains "
              "only lowercase letters, numbers and hyphens. Each slug must "
              "be unique.")


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user)


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created or not Profile.objects.filter(user=instance).count():
        Profile.objects.create(user=instance)
    instance.profile.save()


class DuplicateMixin:
    """
    Helper to duplicate items
    """
    @classmethod
    def get_duplicate_field_names(cls):
        return [field.name for field in cls._meta.get_fields()
                if not field.many_to_many and not field.one_to_many
                and field.name != 'id']

    def duplicate(self):
        model = self.__class__
        dct = {}
        for attr in self.get_duplicate_field_names():
            if attr != 'slug':
                dct[attr] = getattr(self, attr)
        base_slug, idx, dct['slug'] = self.slug + "-", 0, ''
        while not dct['slug']:
            idx += 1
            if not model.objects.filter(slug=base_slug + str(idx)).count():
                dct['slug'] = base_slug + str(idx)
        return model.objects.create(**dct)


class ColorSet(models.Model):
    name = models.CharField(_("Name"), max_length=200)
    slug = models.SlugField(_("Slug"), unique=True, help_text=slug_help,
                            max_length=220)
    background_color = models.CharField(_("Background color"), max_length=20)
    color = models.CharField(_("Color"), max_length=20, default="#000")
    available = models.BooleanField(_("Available"), default=True)

    class Meta:
        verbose_name = _("Color set")
        verbose_name_plural = _("Color sets")
        ordering = ('name',)

    def __str__(self):
        return self.name


class SearchQuery(models.Model):
    name = models.CharField(_("Name"), max_length=200)
    slug = models.SlugField(_("Slug"), unique=True, help_text=slug_help,
                            max_length=220)
    query = models.TextField(_("Query"))
    available = models.BooleanField(_("Available"), default=True)
    default = models.BooleanField(_("Default"), default=True)

    class Meta:
        verbose_name = _("Search query")
        verbose_name_plural = _("Search queries")
        ordering = ('name',)

    def __str__(self):
        return self.name


class ExternalLink(models.Model):
    name = models.CharField(_("Name"), max_length=200)
    link = models.URLField(_("Link"))
    available = models.BooleanField(_("Available"), default=True)

    class Meta:
        verbose_name = _("External link")
        verbose_name_plural = _("External links")
        ordering = ('name',)

    def __str__(self):
        return self.name


class Document(models.Model):
    file = models.FileField(_("File"),
                            upload_to='documents/%Y/%m/')
    name = models.CharField(_("Name"), max_length=200, blank=True, null=True)
    available = models.BooleanField(_("Available"), default=True)

    class Meta:
        verbose_name = _("Document")
        verbose_name_plural = _("Documents")

    @property
    def url(self):
        return self.file.url


class MovieTheater(models.Model):
    name = models.CharField(_("Name"), max_length=200)
    slug = models.SlugField(_("Slug"), unique=True, help_text=slug_help,
                            max_length=220)
    description = models.TextField(_("Description"), blank=True, null=True)
    available = models.BooleanField(_("Available"), default=True)
    default = models.BooleanField(_("Default"), default=False)
    image = models.ImageField(_("Image"), blank=True, null=True)
    email = models.EmailField(_("Email"), blank=True, null=True)
    town = models.CharField(_("Town"), blank=True, null=True, max_length=100)
    intro = models.ForeignKey(FlatPage, verbose_name=_("Intro"), blank=True,
                              null=True, related_name='theater_intros', on_delete=models.SET_NULL)
    footer = models.ForeignKey(FlatPage, verbose_name=_("Footer"), blank=True,
                               null=True, related_name='theater_footers', on_delete=models.SET_NULL)
    side = models.ForeignKey(FlatPage, verbose_name=_("Side"), blank=True,
                             null=True, related_name='theater_sides', on_delete=models.SET_NULL)
    extra_foot = models.TextField(_("Extra foot mention"),
                                  null=True, blank=True)
    header_newsletter = models.TextField(
        _("Newsletter header"), null=True, blank=True,
        help_text=_("You can use markdown syntax"))
    footer_newsletter = models.TextField(
        _("Newsletter footer"), null=True, blank=True,
        help_text=_("You can use markdown syntax"))
    newsletter = models.ForeignKey(
        Newsletter, verbose_name=_("Newsletter"), blank=True, null=True,
        related_name='theaters', on_delete=models.SET_NULL
    )
    site = models.ForeignKey(Site, verbose_name=_("Associated site"),
                             blank=True, null=True, on_delete=models.SET_NULL)
    https_site = models.BooleanField(
        verbose_name=_("Is preferably served by https"), default=False)
    pages = models.ManyToManyField(
        FlatPage, verbose_name=_("Pages"), blank=True,
        related_name='theaters',
        through='TheaterPage'
    )
    external_links = models.ManyToManyField(
        ExternalLink, verbose_name=_("External links"), blank=True,
        related_name='theaters', through='TheaterLink'
    )
    max_day_to_display = models.IntegerField(
        _("Maximum day to display"), default=31)
    facebook_page_access_token = models.CharField(
        _(u"Facebook - Page access token"), blank=True, null=True,
        max_length=500,
        help_text=_(
            "To obtain a page access token follow the instructions on this : "
            "<a href='http://nodotcom.org/python-facebook-tutorial.html'>page"
            "</a>. A script get_facebook_page_token.py is available in the "
            "fantasmagory sources to help getting a page token."
        )
    )
    facebook_app_id = models.CharField(
        _(u"Facebook - Application ID"), blank=True, null=True,
        max_length=100,
        help_text=_("Used for meta information on the page")
    )
    autopublish_day_delay = models.IntegerField(
        _("Facebook - Auto-publish delay in days"), blank=True, null=True,
        help_text=_("Number of day before the broadcast. Could be 0 if are "
                    "published only movies of the day. Facebook page "
                    "token must be filled.")
    )
    autocreate_weekly_mailing = models.BooleanField(
        _(u"Auto create movie mailing"), default=False,
    )
    autopublish_weekly_mailing = models.BooleanField(
        _(u"Auto publish movie mailing"), default=False,
    )
    weekly_mailing_weekday = models.CharField(
        _(u"Week day of the mailing"), default='2',
        max_length=1,
        choices=(('0', _("Monday")), ('1', _("Tuesday")),
                 ('2', _("Wednesday")), ('3', _("Thursday")),
                 ('4', _("Friday")), ('5', _("Saturday")),
                 ('6', _("Sunday"))),
        help_text=_(u"Auto created 3 days before and auto publish 2 days "
                    u"before")
    )
    extra_html = models.TextField(
        _("Extra HTML"), blank=True, null=True,
        help_text=_("Extra HTML code at the end of the page use it for "
                    "instance for tracking code")
    )
    theater_movies = models.BooleanField(_("Offers theater movies"),
                                         default=True)
    digital_movies = models.BooleanField(_("Offers digital movies"),
                                         default=False)

    class Meta:
        verbose_name = _("Movie theater")
        verbose_name_plural = _("Movie theaters")
        ordering = ('name',)

    def __str__(self):
        return self.name


class Ordinal(ChoiceEnum):
    first = 1
    second = 2
    third = 3
    fourth = 4
    fifth = 5
    sixth = 6
    seventh = 7
    eighth = 8
    ninth = 9
    tenth = 10


# mark for translations
(_('first'), _('second'), _('third'), _('fourth'), _('fifth'), _('sixth'),
 _('seventh'), _('eighth'), _('ninth'), _('tenth'), )


class TheaterLink(models.Model):
    theater = models.ForeignKey(MovieTheater, on_delete=models.CASCADE)
    link = models.ForeignKey(ExternalLink, on_delete=models.CASCADE)
    order = models.IntegerField(choices=Ordinal.choices(), default=1)


class TheaterPage(models.Model):
    theater = models.ForeignKey(MovieTheater, on_delete=models.CASCADE)
    page = models.ForeignKey(FlatPage, on_delete=models.CASCADE)
    order = models.IntegerField(choices=Ordinal.choices(), default=1)


class Menu(models.Model):
    theater = models.ForeignKey(MovieTheater, on_delete=models.CASCADE,
                                related_name='menus')
    name = models.CharField(_("Name"), max_length=100)
    slug = models.SlugField(_("Slug"), unique=True, help_text=slug_help,
                            max_length=120)
    available = models.BooleanField(_("Available"), default=True)
    order = models.IntegerField(choices=Ordinal.choices(), default=1)

    class Meta:
        verbose_name = _("Menu")
        verbose_name_plural = _("Menus")
        ordering = ('theater', 'order',)


class MenuPage(models.Model):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE,
                             related_name='pages')
    page = models.ForeignKey(FlatPage, on_delete=models.CASCADE,
                             related_name='menus')
    order = models.IntegerField(choices=Ordinal.choices(), default=1)

    class Meta:
        verbose_name = _("Menu page")
        verbose_name_plural = _("Menu pages")
        ordering = ('menu', 'order',)


class Screen(models.Model):
    movie_theater = models.ForeignKey(MovieTheater, on_delete=models.CASCADE)
    name = models.CharField(_("Name"), max_length=200)
    slug = models.SlugField(_("Slug"), unique=True, help_text=slug_help,
                            max_length=220)
    description = models.TextField(_("Description"), blank=True, null=True)
    available = models.BooleanField(_("Available"), default=True)
    default = models.BooleanField(_("Default"), default=False)

    class Meta:
        verbose_name = _("Screen")
        verbose_name_plural = _("Screens")
        ordering = ('movie_theater', 'name',)

    def __str__(self):
        return "{} - {}".format(self.movie_theater, self.name)


class Image(models.Model):
    movie = models.ForeignKey("Movie", on_delete=models.CASCADE)
    image = models.ImageField(_("Image"),
                              upload_to='images/%Y/%m/')
    name = models.CharField(_("Name"), max_length=200, blank=True, null=True)
    description = models.TextField(_("Description"), blank=True, null=True)

    class Meta:
        verbose_name = _("Image")
        verbose_name_plural = _("Images")


resource_types = (
    ('Youtube', _("Youtube")),
    ('Vimeo', _("Vimeo")),
    ('Dailymotion', _("Dailymotion")),
    ('Video', _("Generic video")),
)

resource_filter = {
    'Dailymotion': (
        [re.compile(r'dailymotion.com/video/([A-Za-z0-9_-]+)')],
        "https://geo.dailymotion.com/player.html?video={}"),
    'Youtube': ((re.compile(r'youtube.com/watch\?['
                            r'A-Za-z0-9_\-\=\&]*v='
                            r'([A-Za-z0-9_-]*)[A-Za-z0-9_\-\=\&]*'),
                 re.compile(r'youtu.be\/([A-Za-z0-9_-]*)'),
                 re.compile(r'youtube.com\/embed\/([A-Za-z0-9_-]*)')),
                "https://www.youtube.com/embed/{}"),
    'Vimeo': ([re.compile(r'vimeo.com\/([0-9]+)')],
              "https://player.vimeo.com/video/{}"),
    'Video': ([re.compile(r'(.*)')], "{}"),
}


class ExternalResource(models.Model):
    movie = models.ForeignKey("Movie", related_name='external_resources', on_delete=models.CASCADE)
    resource_type = models.CharField(_("Type"), default=resource_types[0][0],
                                     max_length=50, choices=resource_types)
    link = models.URLField(_("Link"))
    name = models.CharField(_("Name"), max_length=200, blank=True, null=True)
    description = models.TextField(_("Description"), blank=True, null=True)

    class Meta:
        verbose_name = _("External resource")
        verbose_name_plural = _("External resources")

    @property
    def extra_parameters(self):
        if self.resource_type == 'Youtube':
            return mark_safe("?rel=0&modestbranding=1&showinfo=0")
        if self.resource_type == 'Dailymotion':
            return mark_safe("&autoplay=false")
        return ""

    def get_link(self):
        return self.link + self.extra_parameters

    def save(self, **kwargs):
        # manage iframe for video provider
        if self.resource_type in resource_filter:
            regexps, lnk = resource_filter[self.resource_type]
            key = None
            for regexp in regexps:
                key = regexp.findall(self.link)
                if key:
                    key = key[0]
                    break
            if key:
                self.link = lnk.format(key)
        super().save(**kwargs)


class Genre(models.Model):
    name = models.CharField(_("Name"), max_length=100)
    slug = models.SlugField(_("Slug"), unique=True, help_text=slug_help,
                            max_length=120)
    description = models.TextField(_("Description"), blank=True, null=True)
    available = models.BooleanField(_("Available"), default=True)
    color = models.ForeignKey(ColorSet, verbose_name=_("Associated color"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Genre")
        verbose_name_plural = _("Genres")
        ordering = ('name',)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(_("Name"), max_length=200)
    short_name = models.CharField(_("Short name"), max_length=200, default="",
                                  blank=True)
    slug = models.SlugField(_("Slug"), unique=True, help_text=slug_help,
                            max_length=220)
    display_name = models.CharField(
        _("Display name"), max_length=200, blank=True, null=True,
        help_text=_("Fill it if this tag have a special name for display. "
                    "HTML allowed.")
    )
    order = models.IntegerField(_("Order"), default=10)
    available = models.BooleanField(_("Available"), default=True)
    is_badge = models.BooleanField(_("Is a badge"), default=False)
    description = models.TextField(_("Description"), blank=True, null=True)
    color = models.ForeignKey(ColorSet, verbose_name=_("Associated color"), on_delete=models.CASCADE)

    class Meta:
        abstract = True
        ordering = ("short_name", "name")

    def __str__(self):
        return self.short_name or self.name

    @property
    def display(self):
        return self.display_name or self.name


class MovieTag(Tag):
    class Meta:
        verbose_name = _("Movie tag")
        verbose_name_plural = _("Movie tags")
        ordering = ("order", "short_name", "name")


class Movie(DuplicateMixin, models.Model):
    name = models.CharField(_("Name"), max_length=200)
    slug = models.SlugField(_("Slug"), unique=True, help_text=slug_help,
                            max_length=220)
    poster = models.ImageField(_("Poster"), blank=True, null=True,
                               upload_to='posters/%Y/%m/')
    available = models.BooleanField(_("Available"), default=True)
    duration = models.DurationField(
        _("Duration"), blank=True, null=True,
        help_text=_("Use the format: hours:minutes:seconds. For instance: "
                    "'01:25:00' for a movie duration of one hour and "
                    "twenty-five minutes."))
    technical_description = models.TextField(
        _("Technical description"), blank=True, null=True,
        help_text=_("Put the country of the film, director, actors, release "
                    "date, etc."))
    tags = models.ManyToManyField(MovieTag, blank=True)
    description = models.TextField(_("Synopsis"), blank=True, null=True)
    special_description = models.TextField(
        _("Special description"), blank=True, null=True,
        help_text=_("Special description relative to this movie. For "
                    "instance: programmer comment."))
    color = models.ForeignKey(ColorSet, verbose_name=_("Associated color"),
                              blank=True, null=True, on_delete=models.SET_NULL)
    digital_link = models.URLField(_("Digital link"), blank=True, null=True)
    genres = models.ManyToManyField(Genre, blank=True)

    class Meta:
        verbose_name = _("Movie")
        verbose_name_plural = _("Movies")

    def __str__(self):
        return self.name

    def tags_with_badge(self):
        return self.tags.filter(is_badge=True)

    def tags_without_badge(self):
        return self.tags.filter(is_badge=False)

    def get_absolute_url(self):
        if not self.broadcasts.count():
            return ''
        kwargs = {'slug': self.slug}
        # first broadcast
        br = self.broadcasts.all()[0]
        if not br.screen.movie_theater.default:
            kwargs['theater'] = br.screen.theater.slug
        return reverse('movie-detail-link', kwargs=kwargs)

    def get_description(self):
        desc = " - ".join([str(item) for item in (
                self.duration, self.technical_description,
                self.special_description) if item]) + " - "
        desc += self.description
        return escape(desc)

    @property
    def current_broadcasts(self):
        return [b for b in self.broadcasts.filter(
            date__gte=datetime.date.today())]

    @property
    def old_broadcasts(self):
        return [b for b in self.broadcasts.filter(
            date__lt=datetime.date.today())]

    def first_broadcast_date(self):
        if not self.broadcasts.count():
            return
        return self.broadcasts.order_by('date').all()[0].date
    first_broadcast_date.short_description = _(u"First broadcast")

    def last_broadcast_date(self):
        if not self.broadcasts.count():
            return
        return self.broadcasts.order_by('-date').all()[0].date
    last_broadcast_date.short_description = _(u"Last broadcast")

    def has_image(self):
        return bool(self.poster)
    has_image.boolean = True
    has_image.short_description = _(u"Has image")

    def has_video(self):
        return bool(self.external_resources.count())
    has_video.boolean = True
    has_video.short_description = _(u"Has video")

    @property
    def main_colorset(self):
        """
        Get the main color from genre of the movie
        """
        if self.color:
            return self.color
        colors = {}
        for genre in self.genres.all():
            if genre.color not in colors:
                colors[genre.color] = 0
            colors[genre.color] += 1
        colors = sorted(colors.items(), key=lambda x: x[1], reverse=True)
        if colors:
            return colors[0][0]


class Event(DuplicateMixin, models.Model):
    name = models.CharField(_("Name"), max_length=200)
    slug = models.SlugField(_("Slug"), unique=True, help_text=slug_help,
                            max_length=220)
    short_name = models.CharField(
        _("Name"), max_length=100, blank=True, null=True,
        help_text=_("Short name to be display on the menu and under each movie")
    )
    picture = models.ImageField(_("Picture"), blank=True, null=True,
                                upload_to='events/%Y/%m/')
    available = models.BooleanField(_("Available"), default=True)
    description = models.TextField(_("Description"), blank=True, null=True)
    start_date = models.DateTimeField(_("Start date"))
    end_date = models.DateTimeField(_("End date"), blank=True, null=True)
    color = models.ForeignKey(ColorSet, verbose_name=_("Associated color"), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _("Event")
        verbose_name_plural = _("Events")
        ordering = ('start_date',)

    def __str__(self):
        return self.name

    @property
    def short(self):
        return self.short_name or self.name

    @property
    def old_broadcasts(self):
        return self.broadcasts.filter(date__lt=datetime.date.today()).order_by(
            '-date'
        )

    @property
    def current_broadcasts(self):
        return self.broadcasts.filter(date__gte=datetime.date.today())


class MovieSchedule(models.Model):
    time = models.TimeField(_("Time"))
    available = models.BooleanField(_("Available"), default=True)
    default = models.BooleanField(_("Default"), default=False)

    class Meta:
        verbose_name = _("Movie schedule")
        verbose_name_plural = _("Movie schedules")
        ordering = ('time',)

    def __str__(self):
        return "{:02d}:{:02d}".format(self.time.hour, self.time.minute)


class BroadcastTag(Tag):
    class Meta:
        verbose_name = _("Broadcast tag")
        verbose_name_plural = _("Broadcast tags")
        ordering = ("order", "short_name", "name")


class Broadcast(models.Model):
    date = models.DateField(_("Date"))
    schedule = models.ForeignKey(MovieSchedule, verbose_name=_("Schedule"), on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, related_name='broadcasts', on_delete=models.CASCADE)
    screen = models.ForeignKey(Screen, related_name='broadcasts', on_delete=models.CASCADE)
    booking_url = models.URLField(_("Booking link"), blank=True, default="")
    original_version = models.BooleanField(
        _("Original version"), default=False,
        help_text=_("Set it to true only if the movie is not in native "
                    "language."))
    three_dimensional = models.BooleanField(_("3D"), default=False)
    tags = models.ManyToManyField(BroadcastTag, blank=True)
    events = models.ManyToManyField(Event, related_name='broadcasts',
                                    blank=True)
    facebook_published = models.BooleanField(_("Published on Facebook"),
                                             default=False)
    special_description = models.TextField(
        _("Special description"), blank=True, null=True,
        help_text=_("Special description relative to this broadcast. For "
                    "instance: presence of the director, debate, etc."))

    class Meta:
        verbose_name = _("Broadcast")
        verbose_name_plural = _("Broadcasts")
        ordering = ('date', 'schedule', 'screen')
        unique_together = ("date", "schedule", "screen")

    def __str__(self):
        return "{} - {} - {}".format(self.date, self.screen, self.movie)

    def broadcast_date(self):
        return "{} - {}".format(self.date, self.schedule)

    def tags_with_badge(self):
        return self.tags.filter(is_badge=True)

    def tags_without_badge(self):
        return self.tags.filter(is_badge=False)

    def get_absolute_url(self):
        kwargs = {'slug': self.movie.slug}
        if not self.screen.movie_theater.default:
            kwargs['theater'] = self.screen.movie_theater.slug
        kwargs['broadcast_id'] = self.pk
        return reverse('movie-detail-link', kwargs=kwargs)

    def get_title(self):
        return "{} - {} - {}".format(self.movie, self.date, self.schedule)

    def get_description(self):
        return self.movie.get_description()

    def publish_to_facebook(self, request=None):
        token = self.screen.movie_theater.facebook_page_access_token
        if not token:
            return
        return
        graph = facebook.GraphAPI(token)

        if request:
            protocol = 'https' if request.is_secure() else 'http'
            site = get_current_site(request)
        else:
            theater = self.screen.movie_theater
            site = theater.site
            protocol = 'https' if theater.https_site else 'http'
        current_url = "{}://{}".format(protocol, site.domain)

        attachment = {
            'link': current_url + self.get_absolute_url(),
        }
        graph.put_object("me", "feed", **attachment)
        self.facebook_published = True
        self.save()
