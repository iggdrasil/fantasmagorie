from django.contrib.syndication.views import Feed
from django.utils.translation import ugettext_lazy as _

from fantasmagorie.models import Broadcast, MovieTheater


class LatestEntriesFeed(Feed):
    title = _(u"Latest movies")
    link = "/broadcasts/"

    def get_object(self, request, theater_slug):
        return MovieTheater.objects.get(slug=theater_slug)

    def items(self, obj):
        return Broadcast.objects.filter(
            movie__available=True, screen__movie_theater=obj).order_by(
            '-date')[:60]

    def item_title(self, item):
        return item.get_title()

    def item_description(self, item):
        return item.get_description()
