# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0005_movietheater_max_day_to_display'),
    ]

    operations = [
        migrations.RenameField(
            model_name='broadcasttag',
            old_name='short_name',
            new_name='display_name'
        ),
        migrations.AlterField(
            model_name='broadcasttag',
            name='display_name',
            field=models.CharField(max_length=200, null=True, help_text='Fill it if this tag have a special name for display. HTML allowed.', verbose_name='Display name', blank=True),
        ),
        migrations.RenameField(
            model_name='movietag',
            old_name='short_name',
            new_name='display_name'
        ),
        migrations.AlterField(
            model_name='movietag',
            name='display_name',
            field=models.CharField(max_length=200, null=True, help_text='Fill it if this tag have a special name for display. HTML allowed.', verbose_name='Display name', blank=True),
        )
    ]
