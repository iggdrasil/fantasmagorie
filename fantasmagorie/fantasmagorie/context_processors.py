from .models import MovieTheater
from django.contrib.sites.shortcuts import get_current_site
from django.utils.functional import SimpleLazyObject


def base(request):
    theater = ''
    q = MovieTheater.objects.filter(default=True)
    if q.count():
        theater = q.all()[0]
    protocol = 'https' if request.is_secure() else 'http'
    site = SimpleLazyObject(lambda: get_current_site(request))
    return {
        'site_title': theater,
        'current_url': SimpleLazyObject(
            lambda: "{}://{}".format(protocol, site.domain))
    }
