import logging

from django_extensions.management.jobs import DailyJob

from newsletter.models import Message, Submission
from fantasmagorie.jobs.daily.auto_create_weekly_mailing import \
    get_next_mailing_slugs

logger = logging.getLogger(__name__)


class Job(DailyJob):
    help = "Auto-create daily mailing."

    def execute(self):
        for __, slug, __, __ in get_next_mailing_slugs(
                day_limit=2,
                theater_filter={'autopublish_weekly_mailing': True}):
            q = Message.objects.filter(slug=slug)
            if not q.count():
                # mailing not created
                continue
            message = q.all()[0]
            if Submission.objects.filter(message=message).count():
                # already sent
                continue
            submission = Submission.from_message(message=message)
            submission.prepared = True
            submission.save()
