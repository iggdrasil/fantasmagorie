# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Broadcast',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('date', models.DateField(verbose_name='Date')),
                ('original_version', models.BooleanField(default=False, verbose_name='Original version', help_text='Set it to true only if the movie is not in native language.')),
                ('three_dimensional', models.BooleanField(default=False, verbose_name='3D')),
                ('special_description', models.TextField(null=True, blank=True, verbose_name='Special description', help_text='Special description relative to this broadcast. For instance: presence of the director, debate, etc.')),
            ],
            options={
                'verbose_name_plural': 'Broadcasts',
                'verbose_name': 'Broadcast',
                'ordering': ('date', 'schedule', 'screen'),
            },
        ),
        migrations.CreateModel(
            name='BroadcastTag',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('short_name', models.CharField(null=True, blank=True, verbose_name='Short name', help_text='Fill it if this tag have an abbreviation', max_length=20)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('description', models.TextField(null=True, blank=True, verbose_name='Description')),
            ],
            options={
                'verbose_name_plural': 'Broadcast tags',
                'verbose_name': 'Broadcast tag',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='ColorSet',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('background_color', models.CharField(verbose_name='Background color', max_length=20)),
                ('color', models.CharField(default='#000', verbose_name='Color', max_length=20)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'verbose_name_plural': 'Color sets',
                'verbose_name': 'Color set',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('file', models.FileField(verbose_name='File', upload_to='')),
                ('name', models.CharField(null=True, blank=True, verbose_name='Name', max_length=200)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'verbose_name_plural': 'Documents',
                'verbose_name': 'Document',
            },
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('short_name', models.CharField(null=True, blank=True, verbose_name='Name', help_text='Short name to be display on the menu and under each movie', max_length=100)),
                ('picture', models.ImageField(null=True, blank=True, verbose_name='Picture', upload_to='')),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('description', models.TextField(null=True, blank=True, verbose_name='Description')),
                ('start_date', models.DateTimeField(verbose_name='Start date')),
                ('end_date', models.DateTimeField(null=True, blank=True, verbose_name='End date')),
                ('color', models.ForeignKey(verbose_name='Associated color', to='fantasmagorie.ColorSet', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name_plural': 'Events',
                'verbose_name': 'Event',
                'ordering': ('start_date',),
            },
        ),
        migrations.CreateModel(
            name='ExternalLink',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('link', models.URLField(verbose_name='Link')),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'verbose_name_plural': 'External links',
                'verbose_name': 'External link',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='ExternalResource',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('resource_type', models.CharField(default='Youtube', choices=[('Youtube', 'Youtube')], verbose_name='Type', max_length=10)),
                ('link', models.URLField(verbose_name='Link')),
                ('name', models.CharField(null=True, blank=True, verbose_name='Name', max_length=200)),
                ('description', models.TextField(null=True, blank=True, verbose_name='Description')),
            ],
            options={
                'verbose_name_plural': 'External resources',
                'verbose_name': 'External resource',
            },
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=100)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('description', models.TextField(null=True, blank=True, verbose_name='Description')),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('color', models.ForeignKey(verbose_name='Associated color', to='fantasmagorie.ColorSet', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name_plural': 'Genres',
                'verbose_name': 'Genre',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('image', models.ImageField(verbose_name='Image', upload_to='')),
                ('name', models.CharField(null=True, blank=True, verbose_name='Name', max_length=200)),
                ('description', models.TextField(null=True, blank=True, verbose_name='Description')),
            ],
            options={
                'verbose_name_plural': 'Images',
                'verbose_name': 'Image',
            },
        ),
        migrations.CreateModel(
            name='Movie',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('poster', models.ImageField(null=True, blank=True, verbose_name='Poster', upload_to='')),
                ('description', models.TextField(null=True, blank=True, verbose_name='Description')),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('duration', models.DurationField(null=True, blank=True, verbose_name='Duration', help_text="Use the format: hours:minutes:seconds. For instance: '01:25:00' for a movie duration of one hour and twenty-five minutes.")),
                ('technical_description', models.TextField(null=True, blank=True, verbose_name='Technical description', help_text='Put the country of the film, director, actors, release date, etc.')),
                ('special_description', models.TextField(null=True, blank=True, verbose_name='Special description', help_text='Special description relative to this movie. For instance: programmer comment.')),
                ('genres', models.ManyToManyField(blank=True, to='fantasmagorie.Genre')),
            ],
            options={
                'verbose_name_plural': 'Movies',
                'verbose_name': 'Movie',
            },
        ),
        migrations.CreateModel(
            name='MovieSchedule',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('time', models.TimeField(verbose_name='Time')),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('default', models.BooleanField(default=False, verbose_name='Default')),
            ],
            options={
                'verbose_name_plural': 'Movie schedules',
                'verbose_name': 'Movie schedule',
                'ordering': ('time',),
            },
        ),
        migrations.CreateModel(
            name='MovieTag',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('short_name', models.CharField(null=True, blank=True, verbose_name='Short name', help_text='Fill it if this tag have an abbreviation', max_length=20)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('description', models.TextField(null=True, blank=True, verbose_name='Description')),
                ('color', models.ForeignKey(verbose_name='Associated color', to='fantasmagorie.ColorSet', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name_plural': 'Movie tags',
                'verbose_name': 'Movie tag',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='MovieTheater',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('description', models.TextField(null=True, blank=True, verbose_name='Description')),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('default', models.BooleanField(default=False, verbose_name='Default')),
                ('email', models.EmailField(null=True, blank=True, verbose_name='Email', max_length=254)),
                ('town', models.CharField(null=True, blank=True, verbose_name='Town', max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Movie theaters',
                'verbose_name': 'Movie theater',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Screen',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=200)),
                ('slug', models.SlugField(unique=True, verbose_name='Slug')),
                ('description', models.TextField(null=True, blank=True, verbose_name='Description')),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('default', models.BooleanField(default=False, verbose_name='Default')),
                ('movie_theater', models.ForeignKey(to='fantasmagorie.MovieTheater', on_delete=models.CASCADE)),
            ],
            options={
                'verbose_name_plural': 'Screens',
                'verbose_name': 'Screen',
                'ordering': ('movie_theater', 'name'),
            },
        ),
        migrations.CreateModel(
            name='TheaterLink',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('order', models.IntegerField(choices=[(1, 'first'), (2, 'second'), (3, 'third'), (4, 'fourth'), (5, 'fifth'), (6, 'sixth'), (7, 'seventh'), (8, 'eighth'), (9, 'ninth'), (10, 'tenth')], default=1)),
                ('link', models.ForeignKey(to='fantasmagorie.ExternalLink', on_delete=models.CASCADE)),
                ('theater', models.ForeignKey(to='fantasmagorie.MovieTheater', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='TheaterPage',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('order', models.IntegerField(choices=[(1, 'first'), (2, 'second'), (3, 'third'), (4, 'fourth'), (5, 'fifth'), (6, 'sixth'), (7, 'seventh'), (8, 'eighth'), (9, 'ninth'), (10, 'tenth')], default=1)),
                ('page', models.ForeignKey(to='flatpages.FlatPage', on_delete=models.CASCADE)),
                ('theater', models.ForeignKey(to='fantasmagorie.MovieTheater', on_delete=models.CASCADE)),
            ],
        ),
        migrations.AddField(
            model_name='movietheater',
            name='external_links',
            field=models.ManyToManyField(blank=True, related_name='theaters', verbose_name='External links', to='fantasmagorie.ExternalLink', through='fantasmagorie.TheaterLink'),
        ),
        migrations.AddField(
            model_name='movietheater',
            name='footer',
            field=models.ForeignKey(null=True, blank=True, verbose_name='Footer', related_name='theater_footers', to='flatpages.FlatPage', on_delete=models.SET_NULL),
        ),
        migrations.AddField(
            model_name='movietheater',
            name='intro',
            field=models.ForeignKey(null=True, blank=True, verbose_name='Intro', related_name='theater_intros', to='flatpages.FlatPage', on_delete=models.SET_NULL),
        ),
        migrations.AddField(
            model_name='movietheater',
            name='pages',
            field=models.ManyToManyField(blank=True, related_name='theaters', verbose_name='Pages', to='flatpages.FlatPage', through='fantasmagorie.TheaterPage'),
        ),
        migrations.AddField(
            model_name='movie',
            name='tags',
            field=models.ManyToManyField(blank=True, to='fantasmagorie.MovieTag'),
        ),
        migrations.AddField(
            model_name='image',
            name='movie',
            field=models.ForeignKey(to='fantasmagorie.Movie', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='externalresource',
            name='movie',
            field=models.ForeignKey(to='fantasmagorie.Movie', related_name='external_resources', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='broadcasttag',
            name='color',
            field=models.ForeignKey(verbose_name='Associated color', to='fantasmagorie.ColorSet', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='broadcast',
            name='events',
            field=models.ManyToManyField(blank=True, related_name='broadcasts', to='fantasmagorie.Event'),
        ),
        migrations.AddField(
            model_name='broadcast',
            name='movie',
            field=models.ForeignKey(to='fantasmagorie.Movie', related_name='broadcasts', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='broadcast',
            name='schedule',
            field=models.ForeignKey(verbose_name='Schedule', to='fantasmagorie.MovieSchedule', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='broadcast',
            name='screen',
            field=models.ForeignKey(to='fantasmagorie.Screen', related_name='broadcasts', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='broadcast',
            name='tags',
            field=models.ManyToManyField(blank=True, to='fantasmagorie.BroadcastTag'),
        ),
    ]
