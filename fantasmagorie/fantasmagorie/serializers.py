from rest_framework import serializers
from . import models


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Genre
        fields = ('name', 'slug')


class MovieTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MovieTag
        fields = ('name', 'display_name', 'slug')


class MovieSerializer(serializers.ModelSerializer):
    tags = MovieTagSerializer(many=True, read_only=True)
    genres = GenreSerializer(many=True, read_only=True)

    class Meta:
        model = models.Movie
        fields = ('slug', 'name', 'poster', 'description', 'duration',
                  'technical_description', 'special_description',
                  'genres', 'tags')


class BroadcastTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.BroadcastTag
        fields = ('name', 'display_name', 'slug')


class BroadcastSerializer(serializers.ModelSerializer):
    movie = serializers.SlugRelatedField(
        read_only=True,
        slug_field='slug'
    )
    schedule = serializers.StringRelatedField()
    screen = serializers.SlugRelatedField(
        read_only=True,
        slug_field='slug'
    )
    tags = BroadcastTagSerializer(many=True, read_only=True)

    class Meta:
        model = models.Broadcast
        fields = ('date', 'schedule', 'movie', 'screen', 'original_version',
                  'three_dimensional', 'tags', 'special_description')
