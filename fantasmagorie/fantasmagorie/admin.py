import datetime
from urllib.parse import urlencode

from django.contrib import admin

from django import forms
from django.conf import settings
from django.conf.urls import url
from django.contrib import messages
from django.contrib.admin.views.main import ChangeList
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User
from django.contrib.flatpages.models import FlatPage
from django.db.models import ManyToManyField
from django.db.models import Q
from django.http import HttpResponseRedirect, Http404
from django.template.defaultfilters import date as date_filter
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.csrf import csrf_protect

#from oauth2client.contrib.django_util.storage import DjangoORMStorage
#from google.auth.exceptions import DefaultCredentialsError
#from googleapiclient.discovery import build

from newsletter.admin import SubscriptionAdmin
from newsletter.models import Subscription

from . import models, forms as fantasmagorie_forms, utils


csrf_protect_m = method_decorator(csrf_protect)

admin.site.site_title = _('Fantasmagorie Admin')
admin.site.site_header = _('Fantasmagorie Admin')

admin.site.unregister(FlatPage)


def disable(modeladmin, request, queryset):
    """
    Disable action
    """
    queryset.update(available=False)


disable.short_description = _("Disable")


def enable(modeladmin, request, queryset):
    """
    Enable action
    """
    queryset.update(available=True)


enable.short_description = _("Enable")


def duplicate(modeladmin, request, queryset):
    """
    Duplicate action
    """
    model = queryset.model
    if not hasattr(model, 'duplicate') or not callable(model.duplicate):
        raise NotImplementedError(
            '{} model must implement a duplicate method'.format(model))
    for item in queryset.all():
        item.duplicate()


duplicate.short_description = _("Duplicate")


class DefaultFilterMixIn:
    """
    Manage a default filter in change list.
    To use it inherit from this class and fill default_filters with a list of
    query filter to apply.
    """
    default_filters = []

    def changelist_view(self, request, *args, **kwargs):
        if self.default_filters:
            if not request.META['QUERY_STRING'] and \
                    not request.META.get('HTTP_REFERER', '').startswith(
                        request.build_absolute_uri()):
                c_url = reverse('admin:%s_%s_changelist' % (
                    self.opts.app_label, self.opts.model_name))
                dct = {'today': datetime.date.today().strftime('%Y-%m-%d')}
                filters = []
                for filter in self.default_filters:
                    key = filter.split('=')[0]
                    if key not in request.GET:
                        filters.append(filter.format(**dct))
                if filters:
                    return HttpResponseRedirect(
                        "%s?%s" % (c_url, "&".join(filters)))
        return super(DefaultFilterMixIn, self).changelist_view(request, *args,
                                                               **kwargs)


class CurrentListFilter(admin.SimpleListFilter):
    title = _('currently broadcast')
    parameter_name = 'currently_broadcast'
    date_params = ['broadcasts__date']

    def lookups(self, request, model_admin):
        return (
            ('yes', _('Yes')),
            ('no', _('No')),
        )

    def queryset(self, request, queryset):
        if self.value() == 'yes':
            q = Q()
            for param in self.date_params:
                if not q:
                    q = Q(**{param + '__gte': datetime.date.today()})
                else:
                    q |= Q(**{param + '__gte': datetime.date.today()})
            return queryset.filter(q)
        if self.value() == 'no':
            q = Q()
            for param in self.date_params:
                if not q:
                    q = Q(**{param + '__lt': datetime.date.today()})
                else:
                    q &= Q(**{param + '__lt': datetime.date.today()})
            return queryset.filter(q)


class AdminForm(forms.ModelForm):

    def filter_available(self, attr, cls):
        """
        Filter the choices to only display available items and eventually an
        old item not available anymore.

        :param attr: field name
        :param cls: related class
        """
        q = Q(available=True)
        try:
            if self.instance and hasattr(self.instance, attr):
                attribute = getattr(self.instance, attr)
                if hasattr(attribute, 'pk'):
                    q = q | Q(pk=attribute.pk)
                if hasattr(attribute, 'all'):
                    q = q | Q(pk__in=[obj.pk for obj in attribute.all()])
        except ValueError:
            pass
        self.fields[attr].queryset = cls.objects.filter(q)

    def select_default(self, attr, cls):
        """
        Select the default item.

        :param attr: field name
        :param cls: related class
        """
        q = cls.objects.filter(default=True)
        if q.count():
            self.fields[attr].initial = q.all()[0]

    def hide_lonely(self, attr, cls):
        """
        Hide field if only one item is available
        """
        q = cls.objects
        if q.count() == 1:
            self.fields[attr].widget = forms.HiddenInput()


class TheaterPageInline(admin.TabularInline):
    model = models.TheaterPage
    extra = 2


class TheaterLinkInline(admin.TabularInline):
    model = models.TheaterLink
    extra = 2


@admin.register(models.MovieTheater)
class MovieTheaterAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug', 'available', 'default')
    list_filter = ('available',)
    actions = [enable, disable]
    default_filters = ['available__exact=1']
    inlines = [TheaterLinkInline, TheaterPageInline]


class MenuPageInline(admin.TabularInline):
    model = models.MenuPage
    extra = 2


@admin.register(models.Menu)
class MenuAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'theater', 'available', 'order')
    list_filter = ('available', 'theater')
    actions = [enable, disable]
    inlines = [MenuPageInline]


@admin.register(models.Screen)
class ScreenAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'movie_theater', 'slug', 'available', 'default')
    list_filter = ('available', 'movie_theater')
    actions = [enable, disable]
    default_filters = ['available__exact=1']


class EventForm(AdminForm):
    model = models.Event

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filter_available('color', models.ColorSet)


class CurrentEventListFilter(CurrentListFilter):
    title = _('To come')
    date_params = ['start_date', 'end_date']


@admin.register(models.Event)
class EventAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'available', 'start_date', 'end_date')
    list_filter = (CurrentEventListFilter, 'available', )
    form = EventForm
    actions = [enable, disable, duplicate]
    default_filters = ['currently_broadcast=yes']


class BroadcastForm(AdminForm):
    model = models.Broadcast

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filter_available('screen', models.Screen)
        self.select_default('screen', models.Screen)
        self.hide_lonely('screen', models.Screen)
        self.filter_available('schedule', models.MovieSchedule)
        self.select_default('schedule', models.MovieSchedule)
        self.filter_available('tags', models.BroadcastTag)
        self.filter_available('events', models.Event)


def create_mailing_from_broadcasts(modeladmin, request, queryset):
    # for now we are considering that this movie is broadcast on a single
    # movie theater - multi-theater is not ready for now
    theater = queryset.all()[0].screen.movie_theater
    newsletter = theater.newsletter
    if not newsletter:
        messages.error(request, _("Cannot create a mailing - No newsletter associated to the theater."))
        return

    movies = {}  # list of (first date, movie, broadcasts)
    for broadcast in queryset.all():
        if not broadcast.movie.available:
            continue
        if broadcast.movie in movies:
            movies[broadcast.movie].append(broadcast)
        else:
            movies[broadcast.movie] = [broadcast]

    movies = [
        (movies[movie][0].date, movies[movie][0].schedule.time,
         movie, movies[movie]) for movie in movies
    ]
    message = utils.create_mailing(theater, movies, request)
    if message:
        return HttpResponseRedirect(
            '/admin/newsletter/message/{}/change/'.format(message.pk))


create_mailing_from_broadcasts.short_description = \
    _("Generate a mailing for theses broadcasts")


def post_to_facebook(modeladmin, request, queryset):
    for broadcast in queryset.all():
        broadcast.publish_to_facebook(request)
    messages.info(request,
                  str(_("{} broadcast(s) published to Facebook")).format(
                      queryset.count()))


post_to_facebook.short_description = _("Post to facebook")


class BroadcastInline(admin.TabularInline):
    model = models.Broadcast
    exclude = ["facebook_published"]
    extra = 3
    form = BroadcastForm


class ExternalResourceInline(admin.TabularInline):
    model = models.ExternalResource
    exclude = ["name", "description"]
    extra = 1


class ImageInline(admin.TabularInline):
    model = models.Image
    extra = 1


class MovieForm(AdminForm):
    model = models.Movie

    class Media:
        css = {
            'all': (settings.STATIC_URL + 'admin.css',)
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filter_available('genres', models.Genre)
        self.filter_available('tags', models.MovieTag)


"""
def create_playlist_from_movies(modeladmin, request, queryset,
                                theater=None, title=None, desc=None):
    q = queryset.filter(pk__in=[m.pk for m in queryset.all()],
                        available=True,
                        broadcasts__pk__isnull=False).distinct()
    if not queryset.count() or q.count() != queryset.count():
        if request:
            messages.error(
                request,
                _("Cannot create a playlist - all movies do not have an "
                  "associated broadcast or are not available."))
        return
    if not theater:
        theater = q.all()[0].broadcasts.all()[0].screen.movie_theater

    storage = DjangoORMStorage(
            models.CredentialsModel, 'id', request.user.id, 'credential')
    credentials = storage.get()

    try:
        client = build('youtube', 'v3', credentials=credentials)
    except DefaultCredentialsError:
        return HttpResponseRedirect(reverse('authorize'))
    today = datetime.date.today()
    broadcasts = [
        broadcast
        for broadcast in models.Broadcast.objects.filter(
            movie__pk__in=q.values_list('id', flat=True),
            date__gte=today,
            screen__movie_theater=theater,
        ).order_by('date')
    ]
    if not broadcasts:
        messages.error(request, _(u"Error: playlist not created - no "
                                  u"broadcast to come."))
        return HttpResponseRedirect('/admin/fantasmagorie/movie/')

    if not title:
        title = str(_("{} - Movies from {} to {}")).format(
            theater, date_filter(broadcasts[0].date),
            date_filter(broadcasts[-1].date))
    playlist_id = youtube.create_playlist_from_broadcasts(
        client, broadcasts, title, desc)

    if not playlist_id:
        messages.error(request, _(u"Error: playlist not created."))
    else:
        messages.info(request, str(_(u"Playlist published at: {}{}")).format(
            youtube.YT_PLAYLIST, playlist_id))
    return HttpResponseRedirect('/admin/fantasmagorie/movie/')


create_playlist_from_movies.short_description = \
    _("Generate a youtube playlist for theses movies")
"""

def create_mailing_from_movies(modeladmin, request, queryset,
                               theater=None, date=None):
    q = queryset.filter(
        pk__in=[m.pk for m in queryset.all()], available=True
    ).filter(
        Q(broadcasts__pk__isnull=False) |
        Q(digital_link__isnull=False)).distinct()
    if not queryset.count() or q.count() != queryset.count():
        if request:
            messages.error(
                request,
                _("Cannot create a mailing - all movies do not have an "
                  "associated broadcast or are not available."))
        return
    if not theater:
        for m in q.all():
            if not theater:
                for b in m.broadcasts.all():
                    theater = b.screen.movie_theater
                    break
    if not theater:
        theater = models.MovieTheater.objects.all()

    # get the list of movie sorted by broadcast date
    # usually a newsletter talk about films to be broadcasted and are ordered
    # by broadcast date
    if not date:
        date = datetime.datetime.now()
    movies = []  # list of (first date, movie, broadcasts)
    for movie in q.all():
        q_broadcast = movie.broadcasts.filter(date__gte=date)
        if not q_broadcast.count():
            q_broadcast = movie.broadcasts
        movie_broadcasts = list(
            q_broadcast.order_by('date', 'schedule').all())
        if movie_broadcasts:
            movies.append((movie_broadcasts[0].date,
                           movie_broadcasts[0].schedule.time, movie,
                           movie_broadcasts))
        else:
            movies.append(("", "", movie, []))

    now = datetime.datetime.now()
    message = utils.create_mailing_from_movies(queryset, theater,
                                               date=now, request=request)
    if message and request:
        return HttpResponseRedirect(
            '/admin/newsletter/message/{}/change/'.format(message.pk))


create_mailing_from_movies.short_description = \
    _("Generate a mailing for theses movies")


class ChangeListForChangeView(ChangeList):
    def get_filters_params(self, params=None):
        """
        Get the current list queryset parameters from _changelist_filters
        """
        filtered_params = {}
        lookup_params = super(
            ChangeListForChangeView, self).get_filters_params(params)
        if '_changelist_filters' in lookup_params:
            field_names = [field.name for field in
                           self.model._meta.get_fields()]
            params = lookup_params.pop('_changelist_filters')
            for param in params.split("&"):
                key, value = param.split("=")
                if not value or key not in field_names:
                    continue
                filtered_params[key] = value
        return filtered_params


@admin.register(models.Movie)
class MovieAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    save_on_top = True
    prepopulated_fields = {"slug": ("name",)}
    inlines = (BroadcastInline, ExternalResourceInline,)  # ImageInline,)
    list_display = ('name', 'available', 'first_broadcast_date',
                    'last_broadcast_date', 'has_image', 'has_video')
    list_filter = (CurrentListFilter, 'available', 'genres', 'tags')
    search_fields = ('name',)
    form = MovieForm
    #default_filters = ['currently_broadcast=yes']

    formfield_overrides = {
        ManyToManyField: {'widget': forms.CheckboxSelectMultiple},
    }
    actions = [enable, disable, duplicate, create_mailing_from_movies]
    #           create_playlist_from_movies]

    def get_queryset(self, request):
        return super().get_queryset(request).distinct()

    @csrf_protect_m
    def get_changelist_queryset(self, request):
        """
        Get the changelist queryset to be used in the change view.
        Used by previous and next button.
        Mainly a copy from:
        django/contrib/admin/options.py ModelAdmin->changelist_view
        """
        list_display = self.get_list_display(request)
        list_display_links = self.get_list_display_links(request, list_display)
        list_filter = self.get_list_filter(request)
        search_fields = self.get_search_fields(request)
        list_select_related = self.get_list_select_related(request)

        cl = ChangeListForChangeView(
            request, self.model, list_display,
            list_display_links, list_filter, self.date_hierarchy,
            search_fields, list_select_related, self.list_per_page,
            self.list_max_show_all, self.list_editable, self, self.sortable_by
        )
        return cl.get_queryset(request)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if not extra_context:
            extra_context = {}
        if settings.YOUTUBE_KEY:
            extra_context['search_video'] = True
        ids = list(self.get_changelist_queryset(request).values('pk'))
        previous, current_is_reached, first = None, False, None
        extra_context['get_attr'] = ""
        if request.GET:
            extra_context['get_attr'] = "?" + request.GET.urlencode()
        for v in ids:
            pk = str(v['pk'])
            if pk == object_id:
                current_is_reached = True
                if previous:
                    extra_context['previous_item'] = previous
            elif current_is_reached:
                extra_context['next_item'] = pk
                break
            else:
                if not first:
                    first = pk
                previous = pk
        if 'previous_item' not in extra_context and \
                'next_item' not in extra_context and first:
            # on modify current object do not match current criteria
            # next is the first item
            extra_context['next_item'] = first

        return super().change_view(request, object_id, form_url, extra_context)

    def get_urls(self):
        urls = super(MovieAdmin, self).get_urls()
        my_urls = [
            url(r'^add-broadcasts/(?P<movie_id>[0-9]+)/$',
                permission_required(
                    'fantasmagorie.can_change_movie',
                    '/admin/login/')(self.add_broadcasts)
                ),
            url(r'^fetch-movie/$',
                permission_required(
                    'fantasmagorie.can_change_movie',
                    '/admin/login/')(self.fetch_movie)
                ),
        ]
        if settings.YOUTUBE_KEY:
            my_urls.append(
                url(r'^search-video/(?P<movie_id>[0-9]+)/$',
                    permission_required(
                        'fantasmagorie.can_change_movie',
                        '/admin/login/')(self.search_video)
                    )
            )
        return my_urls + urls

    def add_broadcasts(self, request, movie_id):
        """
        Quick add of broadcasts
        """
        try:
            movie = models.Movie.objects.get(pk=movie_id)
        except models.Movie.DoesNotExists as e:
            return Http404(_("Movie does not exist"))
        if not request.POST:
            form = fantasmagorie_forms.AddBroadcastForm(movie=movie)
        else:
            form = fantasmagorie_forms.AddBroadcastForm(
                request.POST, movie=movie)
            if form.is_valid() and '_add_broadcasts' in request.POST:
                broadcasts = form.save()
                msg = " ; ".join([b.broadcast_date() for b in broadcasts])
                msg = str(
                    _("{} broadcasts added to {}: {}")
                ).format(len(broadcasts), movie, msg)
                messages.info(request, msg)
                url = "/admin/fantasmagorie/movie/{}"\
                      "/change/".format(movie.pk)
                return HttpResponseRedirect(url)
        context = dict(
            self.admin_site.each_context(request),
            movie=movie,
            form=form,
            opts=models.Movie._meta
        )
        return TemplateResponse(request, "admin/add_broadcasts.html", context)

    def search_video(self, request, movie_id):
        """
        Search video on Youtube
        """
        try:
            movie = models.Movie.objects.get(pk=movie_id)
        except models.Movie.DoesNotExists as e:
            return Http404(_("Movie does not exist"))
        coded_query = None
        if not request.POST:
            form = fantasmagorie_forms.SearchMovieForm(
                initial={'query': movie.name}
            )
        else:
            if '_add_video' in request.POST and 'videos' in request.POST:
                link = "https://www.youtube.com/embed/{}".format(
                    request.POST['videos'])
                models.ExternalResource.objects.create(
                    movie=movie,
                    resource_type='Youtube',
                    link=link,
                )
                msg = str(
                    _("One video added to: {}")
                ).format(movie)
                messages.info(request, msg)
                return HttpResponseRedirect(
                    '/admin/fantasmagorie/movie/{}/change/'.format(movie.pk)
                )
            else:
                coded_query = urlencode(
                    {"search_query": request.POST.get("query", "")})
                form = fantasmagorie_forms.SearchMovieForm(request.POST)
        context = dict(
            self.admin_site.each_context(request),
            movie=movie,
            form=form,
            coded_query=coded_query,
            opts=models.Movie._meta
        )
        return TemplateResponse(request, "admin/search_movie.html", context)

    def fetch_movie(self, request):
        """
        Fetch movie from an external database
        """
        form = fantasmagorie_forms.FetchMovieForm
        has_fetched_movies, searched = False, False
        if request.method == 'POST':
            form = form(request.POST)
            if form.is_valid():
                if request.POST.get('_search', None):
                    searched = True
                    has_fetched_movies = form.fetch()
                else:
                    movie = form.save()
                    if movie:
                        return self._post_fetch(request, form, movie)
        else:
            form = form()
        context = dict(
            self.admin_site.each_context(request),
            form=form,
            searched=searched,
            has_fetched_movies=has_fetched_movies,
            opts=models.Movie._meta
        )
        return TemplateResponse(request, "admin/fetch_movie.html", context)

    def _post_fetch(self, request, form, movie):
        if form.potential_duplicates:
            msg = str(
                _("This film is a potential duplicate of: {}. "
                  "Check this movie.")
            ).format(" - ".join(form.potential_duplicates))
            messages.warning(request, msg)
        if form.created_genres:
            if len(form.created_genres) > 1:
                msg = str(
                    _("Genres: {} have been created.")
                ).format(" - ".join(form.created_genres))
            else:
                msg = str(
                    _("Genre: {} has been created.")
                ).format(form.created_genres[0])
            messages.info(request, msg)
        if request.POST.get('_add', None):
            return HttpResponseRedirect(
                '/admin/fantasmagorie/movie/{}/change/'.format(
                    movie.pk))
        msg = str(
            _("The movie: {} has been added")
        ).format(movie)
        messages.info(request, msg)
        return HttpResponseRedirect("/admin/fantasmagorie/movie/fetch-movie/")


@admin.register(models.MovieSchedule)
class MovieScheduleAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    list_display = ('time', 'available', 'default')
    list_filter = ('available',)
    actions = [enable, disable]
    #default_filters = ['available__exact=1']


@admin.register(models.BroadcastTag, models.MovieTag)
class SlugAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'short_name', 'slug', 'order', 'available')
    list_filter = ('available',)
    actions = [enable, disable]
    #default_filters = ['available__exact=1']


@admin.register(models.Genre)
class GenreAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug', 'available', 'color')
    list_filter = ('available', 'color')
    actions = [enable, disable]
    #default_filters = ['available__exact=1']


@admin.register(models.ColorSet)
class ColorSetAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'slug', 'background_color', 'color', 'available')
    list_filter = ('available',)
    actions = [enable, disable]
    #default_filters = ['available__exact=1']


@admin.register(models.ExternalLink)
class ExtLinkAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    list_display = ('name', 'link', 'available')
    list_filter = ('available',)
    actions = [enable, disable]
    #default_filters = ['available__exact=1']


@admin.register(models.Document)
class DocumentAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    list_display = ('name', 'url', 'available')
    list_filter = ('available',)
    actions = [enable, disable]
    #default_filters = ['available__exact=1']


@admin.register(models.FlatPage)
class FlatPageAdmin(admin.ModelAdmin):
    list_display = ('title', 'url',)
    exclude = ('enable_comments', 'template_name', 'registration_required')


class FantasmagorieSubscriptionAdmin(SubscriptionAdmin):
    actions = SubscriptionAdmin.actions + [
        utils.export_as_csv_action(fields=['name', 'email'])]


admin.site.unregister(Subscription)
admin.site.register(Subscription, FantasmagorieSubscriptionAdmin)


class ProfileInline(admin.StackedInline):
    model = models.Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'


class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline, )

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)


class SearchQueryAdmin(admin.ModelAdmin):
    model = models.SearchQuery
    prepopulated_fields = {"slug": ("name",)}
    list_display = ('name', 'available', 'default')
    list_filter = ('available',)
    actions = [enable, disable]


if settings.YOUTUBE_KEY:
    admin.site.register(models.SearchQuery, SearchQueryAdmin)
