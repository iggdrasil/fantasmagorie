# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def migrate_pages(apps, schema_editor):
    FlatPageOld = apps.get_model('flatpages', 'FlatPage')
    FlatPage = apps.get_model('fantasmagorie', 'FlatPage')
    MovieTheater = apps.get_model('fantasmagorie', 'MovieTheater')
    TheaterPage = apps.get_model('fantasmagorie', 'TheaterPage')
    res = {}
    for page in FlatPageOld.objects.all():
        values = {
            'url': page.url,
            'defaults': {
                'title': page.title,
                'content': page.content,
                'enable_comments': page.enable_comments,
                'template_name': page.template_name,
                'registration_required': page.registration_required
            }
        }
        new_page, created = FlatPage.objects.get_or_create(**values)
        res[page.pk] = new_page.pk
        for site in page.sites.all():
            new_page.sites.add(site)
    for theater in MovieTheater.objects.all():
        if theater.intro_id:
            theater.intro_id = res[theater.intro_id]
        if theater.footer_id:
            theater.footer_id = res[theater.footer_id]
        if theater.side_id:
            theater.side_id = res[theater.side_id]
        theater.save()
    for page in TheaterPage.objects.all():
        page.page_id = res[page.page_id]
        page.save()


class Migration(migrations.Migration):

    dependencies = [
        ('fantasmagorie', '0011_auto_20170421_1811'),
    ]

    operations = [
        migrations.RunPython(migrate_pages)
    ]
