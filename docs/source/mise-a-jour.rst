.. -*- coding: utf-8 -*-

===============================
Mise à jour d'une programmation
===============================

:Date: 2020-10-06

Une mise à jour efficace se fait en trois temps :

- mise à jour des films disponibles,
- précisions sur les films, ajout des horaires de projection,
- ajout des bandes annonces.

Après une présentation générale de l'interface d'administration, chaque étape sera détaillée.


Connexion à l'interface et présentation générale
------------------------------------------------

Accès à l'interface
+++++++++++++++++++

Pour accéder à l'interface d'administration, il suffit de taper l'adresse racine du site web et de lui ajouter `/admin/`. Par exemple : `https://cinema-hermine.fr/admin/` .

Cette interface nécessite bien entendu de disposer d'un compte spécifique. Entrez le couple identifiant, mot de passe qui vous a été fournis.

.. image:: static/img/01-login.png



Interface générale
++++++++++++++++++

Une fois celui-ci saisi, vous accédez à l'interface générale d'administration.

.. image:: static/img/02-interface-generale.png


Sur le bandeau bleu foncé en haut à droite, vous avez accés à des liens :

- « Voir le site » pour retourner au site principal.
- « Changer de mot de passe » pour modifier le mot de passe de son compte.
- « Déconnexion » afin de terminer sa session de travail en administration. Si vous utilisez un ordinateur public, il est impératif de se déconnecter une fois la session de travail terminée.

Ensuite sous le bandeau bleu clair « FANTASMAGORIE », les types d'éléments sur lesquels vous pouvez intervenir sont listés. Selon les droits que vous avez en administration cette liste peut changer.

Dans le cadre d'une mise à jour de programmation, seul l'élément « Films » est pertinent.
Pour accéder à la liste des films en base de données, au niveau de la ligne « Films » cliquer sur « Films » ou « Modifier » pour accéder à la liste des films.

Liste d'éléments
++++++++++++++++

.. image:: static/img/03-liste-films.png

En haut à gauche sur le bandeau bleu clair, un fil d'ariane reprends à quel type d'élément on accède. Cliquer sur « Accueil » permet de revenir à la page précédent, « Fantasmagorie » est l'application, cliquer dessus permet de se rendre dans la liste des éléments de l'application (en l'occurrence avec un compte n'ayant accès qu'aux films, la page est similaire à la page d'accueil).

La liste des films se présente sous la forme d'un tableau. Par défaut, la liste des films est ordonnée par ordre d'ajout en base de données de manière anté-chronologique (le dernier film ajouté apparraît en premier).

Une zone de recherche permet de filtrer les résultats par nom de film.

Sur la droite en dessous du bandeau « Filtre », la liste des films peut-être filtrée selon différents critères. Par exemple le filtre « actuellement diffusé » permet de filtrer sur les films disposant d'une date de diffusion dans le futur. Cliquer sur un critère filtre immédiatement la liste. Pour enlever un filtre, il suffit de cliquer sur « Tout ». 

Depuis cette liste, différentes actions sont possibles. Ces actions sont détaillées dans la section suivante de la documentation.

Pour accéder au formulaire d'un film, il suffit de cliquer sur le nom du film.

Pour ajouter des films deux boutons arrondis gris en haut à droite sont à disposition : « Ajouter film » et « Rechercher en ligne ».

En sélectionnant plusieurs films avec la case à cocher, il est possible de faire des actions groupées via la liste déroulante « Action ».


Mise à jour des films disponibles
---------------------------------

TODO

Précisions sur les films, ajout des horaires de projection
----------------------------------------------------------


TODO

Ajout des bandes annonces
-------------------------
