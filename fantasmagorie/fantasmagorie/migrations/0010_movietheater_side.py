# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0001_initial'),
        ('fantasmagorie', '0009_auto_20170111_1803'),
    ]

    operations = [
        migrations.AddField(
            model_name='movietheater',
            name='side',
            field=models.ForeignKey(blank=True, to='flatpages.FlatPage', verbose_name='Side', null=True, related_name='theater_sides', on_delete=models.SET_NULL),
        ),
    ]
