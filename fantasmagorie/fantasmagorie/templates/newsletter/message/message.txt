{% load i18n %}+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

{{ newsletter.title|safe }}{% trans ": " %}{{ message.title|safe }}

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

{% for article in message.articles.all %}
{{ article.title|striptags|safe }}
=============================================================================
{{ article.text|striptags|safe }}

{% endfor %}

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
{% if submission.publish %}
{% trans "Read message online:" %} http://{{ site.domain }}{{ submission.get_absolute_url }}
{% endif %}{% trans "Unsubscribe:" %} http://{{ site }}{% url "newsletter_unsubscribe_request" newsletter.slug %}
