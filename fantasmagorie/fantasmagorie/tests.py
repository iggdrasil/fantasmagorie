from bs4 import BeautifulSoup
import datetime
import os
from rest_framework.test import APITestCase

from django.conf import settings
from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from newsletter.models import Newsletter
from . import models, admin


class MockRequest(object):
    pass

request = MockRequest()


class ModelTests(TestCase):
    def test_youtubelink(self):
        movie = models.Movie.objects.create(name="Test", slug='test')
        res = models.ExternalResource.objects.create(
            movie=movie, resource_type='Youtube',
            link="https://www.youtube.com/watch?v=j-WcSnin6SA")
        self.assertEqual(res.link,
                         "https://www.youtube.com/embed/j-WcSnin6SA")


class GenericTests(TestCase):
    # for modeladmin inspired by:
    # https://github.com/django/django/blob/master/tests/modeladmin/tests.py
    fixtures = ['test-data.json']
    models = [models.Broadcast, models.BroadcastTag, models.Event,
              models.Genre, models.Movie, models.MovieSchedule,
              models.ColorSet, models.MovieTheater, models.MovieTag,
              models.Screen, models.ExternalLink]

    def setUp(self):
        self.site = AdminSite()
        password = 'mypassword'
        my_admin = User.objects.create_superuser(
            'myuser', 'myemail@test.com', password)
        self.client = Client()
        self.client.login(username=my_admin.username, password=password)

    def test_status(self):
        response = self.client.get(reverse('status'))
        self.assertEqual(response.status_code, 200)

    def test_listing_and_detail(self):
        for model in self.models:
            # quick test to verify basic access to listing
            base_url = '/admin/fantasmagorie/{}/'.format(model.__name__.lower())
            url = base_url
            response = self.client.get(url)
            if model in [models.Event, models.Movie]:
                # by default filter available items
                url += "?currently_broadcast=yes"
                #self.assertRedirects(response, url)
                response = self.client.get(url)
            elif 'available' in [f.name for f in model._meta.get_fields()]:
                # by default filter available items
                url += "?available__exact=1"
                #self.assertRedirects(response, url)
                response = self.client.get(url)
            self.assertEqual(
                response.status_code, 200,
                msg="Can not access admin list for {}.".format(model))
            url = base_url + "{}/change/".format(model.objects.all()[0].pk)
            response = self.client.get(url)
            self.assertEqual(
                response.status_code, 200,
                msg="Can not access admin detail for {}.".format(model))

    def test_fetch_movie(self):
        movie_nb = models.Movie.objects.count()
        url = '/admin/fantasmagorie/movie/fetch-movie/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(url, {"movie": "upstream color",
                                          "_search": "1"})
        self.assertEqual(response.status_code, 200)
        # 2013-04-05 date of the release of this movie
        self.assertIn("2013-04-05", response.content.decode('utf-8'))
        response = self.client.post(url, {"movie": "upstream color",
                                          "fetched_movies": "145197",
                                          "_add": "1"})
        self.assertEqual(movie_nb + 1, models.Movie.objects.count())

        new_url = '/admin/fantasmagorie/movie/{}/change/'.format(
            models.Movie.objects.order_by("-pk").all()[0].pk)
        self.assertRedirects(response, new_url)

        models.Genre.objects.filter(slug="science-fiction").delete()
        # resubmit -> a duplicate warning should be raise
        response = self.client.post(url, {"movie": "upstream color",
                                          "fetched_movies": "145197",
                                          "_add": "1"}, follow=True)
        content = response.content.decode('utf-8')
        self.assertTrue("potential duplicate of" in content or
                        "potentiel doublon de" in content)
        # a new genre have been created
        self.assertTrue("Fiction has been created" in content or
                        "Fiction a été créé" in content)

    def test_str(self):
        # test __str__
        for model in self.models:
            self.assertTrue(str(model.objects.all()[0]))

    def test_abs_url(self):
        # test __str__
        for model in self.models:
            obj = model.objects.all()[0]
            if hasattr(obj, 'get_absolute_url'):
                self.assertTrue(obj.get_absolute_url())

    """
    def test_filter_available(self):
        adm = admin.BroadcastAdmin(models.Broadcast, self.site)
        form = adm.get_form(None)()
        # 2 choices: one screen available and the default empty
        self.assertEqual(len(list(form.fields['screen'].choices)), 2)
        # but if the broadcast is associated with a screen not available
        # anymore, it must be in this choices
        q = models.Broadcast.objects.filter(screen__available=False)
        old_broadcast = q.all()[0]
        form = adm.get_form(request)(instance=old_broadcast)
        self.assertEqual(len(list(form.fields['screen'].choices)), 3)

    def test_select_default(self):
        adm = admin.BroadcastAdmin(models.Broadcast, self.site)
        form = adm.get_form(None)()
        soup = BeautifulSoup(str(form), "html.parser")
        select = soup.find("select", {"id": "id_schedule"})
        self.assertIn("selected",  str(select.find("option", {"value": "4"})))
    """

    def test_duplicate(self):
        movie_nb = models.Movie.objects.count()
        movie = models.Movie.objects.all()[0]
        alt_movie = movie.duplicate()
        self.assertEqual(movie_nb + 1, models.Movie.objects.count())
        for attr in models.Movie.get_duplicate_field_names():
            if attr == 'slug':
                self.assertEqual(alt_movie.slug, movie.slug + '-1')
            else:
                self.assertEqual(getattr(movie, attr),
                                 getattr(alt_movie, attr))


class APITests(APITestCase):
    fixtures = ['test-data.json']

    def test_get_broadcast_list(self):
        response = self.client.get(
            reverse('api-broadcast-list'), format='json')
        self.assertEqual(response.status_code, 200)
        # not date provided: today is used, one old movie is filtered
        # a broadcast for a non available movie should not be displayed!
        self.assertEqual(len(response.data), 10)
        # the old movie is back!
        response = self.client.get(
            reverse('api-broadcast-list', kwargs={'start_date': '1982-01-01'}),
            format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 13)
        # old movie only
        response = self.client.get(
            reverse('api-broadcast-list', kwargs={'start_date': '1982-01-01',
                                                  'end_date': '2017-01-01'}),
            format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)

    def test_get_movie_detail(self):
        response = self.client.get(
            reverse('api-movie-detail', kwargs={'slug': 'blade-runner'}),
            format='json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.data)
        response = self.client.get(
            reverse('api-movie-detail', kwargs={'slug': 'lost-movie'}),
            format='json')
        # an unavailable movie is... not available
        self.assertEqual(response.status_code, 404)


class PageTests(TestCase):
    fixtures = ['test-data.json']

    def tearDown(self):
        fle = os.path.join(settings.MEDIA_ROOT, 'zorglub4242', "styles.css")
        if os.path.isfile(fle):
            os.remove(fle)
        fle = os.path.join(settings.MEDIA_ROOT, 'zorglub4242', "favicon.ico")
        if os.path.isfile(fle):
            os.remove(fle)
        dir = os.path.join(settings.MEDIA_ROOT, "zorglub4242")
        if os.path.isdir(dir):
            os.rmdir(dir)

    def test_index_future(self):
        for broadcast in models.Broadcast.objects.filter(
                date__gte=datetime.date.today()).all():
            broadcast.delete()
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_index_no_movie(self):
        for broadcast in models.Broadcast.objects.all():
            broadcast.delete()
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_index(self):
        # silently display default page on incoherent date
        response = self.client.get(
            reverse(
                'index',
                kwargs={'theater': models.MovieTheater.objects.all()[0].slug,
                        'current_date': '2016-42-55'}))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content.decode('utf-8'), "html.parser")
        movies = soup.find_all("a", class_="movie-link")
        self.assertEqual(len(movies), 8)
        events = soup.find_all("a", class_="event-link")
        # events: top menu
        self.assertEqual(len(events), 2)
        # test some days in calendar display
        days = list(soup.find_all("span", class_="day"))
        # test with a broadcast on monday
        br = models.Broadcast.objects.filter(movie__available=True).all()[0]
        br.date = datetime.date(2050, 11, 21)
        br.save()
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

        # extra css - favicon
        models.MovieTheater.objects.create(name='Zorglub',
                                           slug='zorglub4242')
        response = self.client.get(reverse('index',
                                           kwargs={'theater': 'zorglub4242'}))
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("zorglub4242/styles.css",
                         response.content.decode('utf-8'))
        os.makedirs(os.path.join(settings.MEDIA_ROOT, 'zorglub4242'))
        open(os.path.join(settings.MEDIA_ROOT, 'zorglub4242', "styles.css"),
             'a').close()
        open(os.path.join(settings.MEDIA_ROOT, 'zorglub4242', "favicon.ico"),
             'a').close()
        response = self.client.get(reverse('index',
                                           kwargs={'theater': 'zorglub4242'}))
        self.assertEqual(response.status_code, 200)
        self.assertIn("zorglub4242/styles.css",
                      response.content.decode('utf-8'))
        self.assertIn("zorglub4242/favicon.ico",
                      response.content.decode('utf-8'))
        # must have a default movie theater
        theater = models.MovieTheater.objects.get(default=True)
        theater.default = False
        theater.save()
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 404)

    def test_movie_detail(self):
        response = self.client.get(
            reverse('movie-detail', kwargs={"slug": 'moi-daniel-blake'}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            reverse('movie-detail', kwargs={"slug": 'docteur-strange'}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            reverse('movie-detail', kwargs={"slug": 'blade-runner'}))
        self.assertEqual(response.status_code, 200)
        brid = models.Broadcast.objects.filter(
            movie__slug='blade-runner').all()[0].pk
        response = self.client.get(
            reverse('movie-detail', kwargs={"slug": 'blade-runner',
                                            "broadcast_id": brid}))
        self.assertEqual(response.status_code, 200)
        old_movie = models.Movie.objects.get(slug='lost-movie')
        old_movie.available = False
        old_movie.save()
        response = self.client.get(
            reverse('movie-detail', kwargs={"slug": 'lost-movie'}))
        self.assertEqual(response.status_code, 404)
        old_movie.available = True
        old_movie.save()
        response = self.client.get(
            reverse('movie-detail', kwargs={"slug": 'lost-movie'}))
        self.assertEqual(response.status_code, 200)

    def test_movie_link_detail(self):
        response = self.client.get(
            reverse('movie-detail-link', kwargs={"slug": 'moi-daniel-blake'}))
        self.assertEqual(response.status_code, 200)

    def test_old_movie_link_detail(self):
        response = self.client.get(
            reverse('movie-detail-link', kwargs={"slug": 'pretty-old-movie'}))
        self.assertEqual(response.status_code, 200)

    def test_unavailable_movie_link_detail(self):
        response = self.client.get(
            reverse('movie-detail-link', kwargs={"slug": 'lost-movie'}))
        self.assertEqual(response.status_code, 404)

    def test_event_link_detail(self):
        response = self.client.get(
            reverse('event-detail-link', kwargs={"slug": 'fete-du-cinema'}))
        self.assertEqual(response.status_code, 200)

    def test_event_detail(self):
        response = self.client.get(
            reverse('event-detail', kwargs={"slug": 'fete-du-cinema'}))
        self.assertEqual(response.status_code, 200)
        event = models.Event.objects.get(slug='fete-du-cinema')
        event.available = False
        event.save()
        response = self.client.get(
            reverse('event-detail', kwargs={"slug": 'fete-du-cinema'}))
        self.assertEqual(response.status_code, 404)

    def test_flatpage(self):
        response = self.client.get(reverse('flatpage',
                                           kwargs={'url': '/pratique/'}))
        self.assertEqual(response.status_code, 200)
        page = models.FlatPage.objects.get(url="/pratique/")
        models.TheaterPage.objects.get(theater__default=True,
                                       page__url="/pratique/").delete()
        response = self.client.get(reverse('flatpage',
                                           kwargs={'url': '/pratique/'}))
        self.assertEqual(response.status_code, 200)  # always present in menu
        models.MenuPage.objects.get(page__url="/pratique/").delete()
        response = self.client.get(reverse('flatpage',
                                           kwargs={'url': '/pratique/'}))
        self.assertEqual(response.status_code, 404)


class ExportTests(TestCase):
    fixtures = ['test-data.json']

    def test_ical(self):
        brid = models.Broadcast.objects.filter(
            movie__slug='blade-runner').all()[0].pk
        response = self.client.get(
            reverse('movie-ical', kwargs={"slug": 'blade-runner',
                                          "broadcast_id": brid}))
        self.assertEqual(response.status_code, 200)
        brid = models.Broadcast.objects.filter(
            movie__slug='moi-daniel-blake').all()[0].pk
        response = self.client.get(
            reverse('movie-ical', kwargs={"slug": 'moi-daniel-blake',
                                          "broadcast_id": brid}))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(
            reverse('movie-ical', kwargs={"slug": 'moi-daniel-blake',
                                          "broadcast_id": 9999}))
        self.assertEqual(response.status_code, 404)


class NewsletterTests(TestCase):
    fixtures = ['test-data.json']

    def setUp(self):
        password = 'mypassword'
        my_admin = User.objects.create_superuser(
            'myuser', 'myemail@test.com', password)
        self.client = Client()
        self.client.login(username=my_admin.username, password=password)

    def test_newsletter_generation_from_movies(self):
        movie_url = '/admin/fantasmagorie/movie/?currently_broadcast=yes'

        data = {'action': 'create_mailing_from_movies',
                '_selected_action': [str(m.pk) for m in
                                     models.Movie.objects.all()]}
        response = self.client.post(movie_url, data, follow=True)
        msg = str(_("Cannot create a mailing - all movies do not have an "
                    "associated broadcast or are not available.")).replace("'", "&#39;")
        content = response.content.decode('utf-8')
        self.assertIn(msg, content)

        data = {'action': 'create_mailing_from_movies',
                '_selected_action': [str(m.pk) for m in
                                     models.Movie.objects.filter(
                                         available=True).all()]}
        response = self.client.post(movie_url, data, follow=True)
        msg = str(_("Cannot create a mailing - No newsletter "
                    "associated to the theater.")).replace("'", "&#39;")
        content = response.content.decode('utf-8')
        self.assertIn(msg, content)

        newsletter = Newsletter.objects.create(
            title="Test newsletter", slug="test-nl",
            email="sender@example.com",
            sender="I am the sender man",
        )
        theater = models.MovieTheater.objects.all()[0]
        theater.newsletter = newsletter
        theater.header_newsletter = "### myheader h3"
        theater.footer_newsletter = "myfooter"
        theater.save()
        response = self.client.post(movie_url, data, follow=True)
        msg = str(_("Newsletter created.")).replace("'", "&#39;")
        content = response.content.decode('utf-8')
        self.assertIn(msg, content)

    """
    def test_newsletter_generation_from_broadcasts(self):
        newsletter = Newsletter.objects.create(
            title="Test newsletter", slug="test-nl",
            email="sender@example.com",
            sender="I am the sender man",
        )
        theater = models.MovieTheater.objects.all()[0]
        theater.newsletter = newsletter
        theater.header_newsletter = "### myheader h3"
        theater.footer_newsletter = "myfooter"
        theater.save()

        broadcast_url = '/admin/fantasmagorie/broadcast/'

        data = {'action': 'create_mailing_from_broadcasts',
                '_selected_action': [str(b.pk) for b in
                                     models.Broadcast.objects.all()]}
        response = self.client.post(broadcast_url, data, follow=True)
        msg = str(_("Cannot create a mailing - all movies do not have an "
                    "associated broadcast or are not available.")).replace("'", "&#39;")
        content = response.content.decode('utf-8')
        self.assertIn(msg, content)
        models.Movie.objects.update(available=True)
        msg = str(_("Newsletter created.")).replace("'", "&#39;")
        response = self.client.post(broadcast_url, data, follow=True)
        content = response.content.decode('utf-8')
        self.assertIn(msg, content)
    """
