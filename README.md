Fantasmagorie
=============
[![build status](https://gitlab.com/nimn/fantasmagorie/badges/master/build.svg)](https://gitlab.com/nimn/fantasmagorie/commits/master)
[![coverage report](https://gitlab.com/nimn/fantasmagorie/badges/master/coverage.svg)](https://gitlab.com/nimn/fantasmagorie/commits/master)

Fantasmagorie is software for helping the management of small movie theaters.
For now it helps manage a (nice) public website that displays movies to come.
